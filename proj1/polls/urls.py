from django.urls import path
from . import views
# import views
urlpatterns = [
    path('home/', views.home, name='home'),
    path('', views.home, name='home'),
    path('index/', views.home, name='home'),
    path('contact', views.contact),
    path('a_propos', views.about),
    path('about', views.about, name='about'),





]